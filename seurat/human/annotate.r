#! /usr/bin/env Rscript

cat(format(Sys.time(), "[%H:%M:%S]"), "Loading libraries\n")

# environment vars
suppressPackageStartupMessages(library(methods))
suppressPackageStartupMessages(library(Seurat))
suppressPackageStartupMessages(library(readr))
suppressPackageStartupMessages(library(tibble))
suppressPackageStartupMessages(library(Matrix))
suppressPackageStartupMessages(library(yaml))
suppressPackageStartupMessages(library(ggExtra))
suppressPackageStartupMessages(library(grid))
suppressPackageStartupMessages(library(gridExtra))
suppressPackageStartupMessages(library(ggplot2))

########################## FUNCTIONS ##########################
# Convenience function to make directory if it doesn't already exist
make_dir <- function(dir) {
  if (!dir.exists(dir)) {
    dir.create(dir)
  }
}

# Convenience function to timestamp output
cat_time <- function(x) {
  cat(format(Sys.time(), "[%H:%M:%S]"), x)
}
catf <- function(format, ...) {
  cat_time(sprintf(format, ...))
}

########################## ADS ##########################
cat_time("Annotating Ads\n")
ads <- readRDS("./seurat/human/ads/var_genes_2000/int_method_RPCA/PCs_20/res_0.6/seurat.rds")
ad1 <- WhichCells(ads, idents = c(10, 11))
ad2 <- WhichCells(ads, idents = c(2, 5, 6))
ad3 <- WhichCells(ads, idents = c(7, 8))
ad4 <- WhichCells(ads, idents = 9)
ad5 <- WhichCells(ads, idents = 1)
ad6 <- WhichCells(ads, idents = 3)
ad7 <- WhichCells(ads, idents = 4)
ads$cell_type <- "temp"
ads$cell_type[ad1] <- "hAd1"
ads$cell_type[ad2] <- "hAd2"
ads$cell_type[ad3] <- "hAd3"
ads$cell_type[ad4] <- "hAd4"
ads$cell_type[ad5] <- "hAd5"
ads$cell_type[ad6] <- "hAd6"
ads$cell_type[ad7] <- "hAd7"

Idents(ads) <- "cell_type"
DefaultAssay(ads) <- "RNA"
ads <- NormalizeData(ads)
ads$bmi2 <- "Over 30"
cells <- WhichCells(ads, expression = bmi < 30)
ads$bmi2[cells] <- "Below 30"
saveRDS(ads, "human_ads.rds")

########################## PREADS ##########################
cat_time("Annotating Preads\n")
preads <- readRDS("./seurat/human/preads/var_genes_1500/int_method_RPCA/PCs_20/res_0.4/seurat.rds")
pre1 <- WhichCells(preads, idents = c(3, 9, 10))
pre2 <- WhichCells(preads, idents = c(2, 4))
pre3 <- WhichCells(preads, idents = c(6, 7))
pre4 <- WhichCells(preads, idents = 8)
pre5 <- WhichCells(preads, idents = 5)
pre6 <- WhichCells(preads, idents = 1)
endomet <- WhichCells(end, idents = 2)
preads$cell_type <- "temp"
preads$cell_type[pre1] <- "hASPC1"
preads$cell_type[pre2] <- "hASPC2"
preads$cell_type[pre3] <- "hASPC3"
preads$cell_type[pre4] <- "hASPC4"
preads$cell_type[pre5] <- "hASPC5"
preads$cell_type[pre6] <- "hASPC6"

cells <- WhichCells(preads, expression = cell_type == "temp", invert = TRUE)
preads <- subset(preads, cells = cells)
preads <- RunUMAP(preads, dims = 1:20, return.model = T)
Idents(preads) <- "cell_type"
DefaultAssay(preads) <- "RNA"
preads <- NormalizeData(preads)
preads$bmi2 <- "Over 30"
cells <- WhichCells(preads, expression = bmi < 30)
preads$bmi2[cells] <- "Below 30"
saveRDS(preads, "human_preads.rds")

########################## MES ##########################
cat_time("Annotating Mes\n")
mes <- readRDS("./seurat/human/mes/var_genes_1000/int_method_RPCA/PCs_10/res_0.1/seurat.rds")
mes1 <- WhichCells(mes, idents = 2)
mes2 <- WhichCells(mes, idents = 3)
mes3 <- WhichCells(mes, idents = 1)
mes$cell_type <- "temp"
mes$cell_type[mes1] <- "hMes1"
mes$cell_type[mes2] <- "hMes2"
mes$cell_type[mes3] <- "hMes3"

DefaultAssay(mes) <- "RNA"
mes <- NormalizeData(mes)
mes$bmi2 <- "Over 30"
cells <- WhichCells(mes, expression = bmi < 30)
mes$bmi2[cells] <- "Below 30"
saveRDS(mes, "human_mes.rds")

########################## VASC ##########################
cat_time("Annotating Vascular Cells\n")
vasc <- readRDS("./seurat/human/vasc/var_genes_1000/int_method_RPCA/PCs_30/res_0.5/seurat.rds")
ven <- WhichCells(vasc, idents = c(10, 11, 12))
stalk1 <- WhichCells(vasc, idents = 4)
stalk2 <- WhichCells(vasc, idents = 5)
stalk3 <- WhichCells(vasc, idents = 13)
art1 <- WhichCells(vasc, idents = 8)
art2 <- WhichCells(vasc, idents = 9)
peri <- WhichCells(vasc, idents = 2)
sm1 <- WhichCells(vasc, idents = 1)
sm2 <- WhichCells(vasc, idents = 3)
lec <- WhichCells(vasc, idents = 6)
lec2 <- WhichCells(vasc, idents = 7)

vasc$cell_type <- "temp"
vasc$cell_type[sm1] <- "hSMC1"
vasc$cell_type[sm2] <- "hSMC2"
vasc$cell_type[peri] <- "hPeri"
vasc$cell_type[lec] <- "hLEC1"
vasc$cell_type[lec2] <- "hLEC2"
vasc$cell_type[ven] <- "hEndoV"
vasc$cell_type[stalk1] <- "hEndoS1"
vasc$cell_type[stalk2] <- "hEndoS2"
vasc$cell_type[stalk3] <- "hEndoS3"
vasc$cell_type[art1] <- "hEndoA1"
vasc$cell_type[art2] <- "hEndoA2"

cells <- WhichCells(vasc, expression = cell_type == "temp", invert = TRUE)
vasc <- subset(vasc, cells = cells)
vasc <- RunUMAP(vasc, dims = 1:30, return.model = T)
Idents(vasc) <- "cell_type"
DefaultAssay(vasc) <- "RNA"
vasc <- NormalizeData(vasc)
vasc$bmi2 <- "Over 30"
cells <- WhichCells(vasc, expression = bmi < 30)
vasc$bmi2[cells] <- "Below 30"
saveRDS(vasc, "human_vasc.rds")

########################## IMM ##########################
cat_time("Annotating Immune Cells\n")
immune <- readRDS("./seurat/human/imm/var_genes_1500/int_method_RPCA/PCs_30/res_0.3/seurat.rds")
mast <- WhichCells(immune, idents = 8)
DC2 <- WhichCells(immune, idents = 1)
DC3 <- WhichCells(immune, idents = 2)
t1 <- WhichCells(immune, idents = 5)
t2 <- WhichCells(immune, idents = 6)
t3 <- WhichCells(immune, idents = 3)
nk <- WhichCells(immune, idents = 4)
macs <- readRDS("./seurat/human/imm/macs/var_genes_1500/int_method_RPCA/PCs_30/res_0.4/seurat.rds")
mac1 <- WhichCells(macs, idents = c(7, 8, 9))
mac2 <- WhichCells(macs, idents = c(3, 10))
mac3 <- WhichCells(macs, idents = c(11, 12))
DC1 <- WhichCells(macs, idents = 4)
mono <- WhichCells(macs, idents = 5)
neu <- WhichCells(macs, idents = 2)
mono2 <- WhichCells(macs, idents = 1)
bcell <- readRDS("./seurat/human/imm/bcell/var_genes_1500/int_method_RPCA/PCs_30/res_0.2/seurat.rds")
bcell1 <- WhichCells(bcell, idents = 2)
bcell2 <- WhichCells(bcell, idents = 1)
bcell3 <- WhichCells(bcell, idents = 3)

immune$cell_type <- "temp"
immune$cell_type[mast] <- "hMast"
immune$cell_type[bcell1] <- "hBcell"
immune$cell_type[bcell2] <- "hpDC"
immune$cell_type[bcell3] <- "hPlasmablast"
immune$cell_type[mac1] <- "hMac1"
immune$cell_type[mac2] <- "hMac2"
immune$cell_type[mac3] <- "hMac3"
immune$cell_type[mono] <- "hMono1"
immune$cell_type[DC1] <- "hcDC2"
immune$cell_type[DC2] <- "hcDC1"
immune$cell_type[DC3] <- "hASDC"
immune$cell_type[mono2] <- "hMono2"
immune$cell_type[t1] <- "hTcell1"
immune$cell_type[t2] <- "hTcell2"
immune$cell_type[t3] <- "hTreg"
immune$cell_type[nk] <- "hNK"
immune$cell_type[neu] <- "hNeu"

cells <- WhichCells(immune, expression = cell_type == "temp", invert = TRUE)
immune <- subset(immune, cells = cells)
immune <- RunUMAP(immune, dims = 1:30, return.model = T)
Idents(immune) <- "cell_type"
DefaultAssay(immune) <- "RNA"
immune <- NormalizeData(immune)
immune$bmi2 <- "Over 30"
cells <- WhichCells(immune, expression = bmi < 30)
immune$bmi2[cells] <- "Below 30"
saveRDS(immune, "human_immune.rds")
# myeloid <- subset(immune, idents = c("hMac1", "hMac2", "hMac3", "hMono1", "hMono2", "hcDC1", "hcDC2", "hASDC", "hMast", "hNeu"))
# DefaultAssay(myeloid) <- "integrated"
# myeloid <- RunPCA(myeloid)
# myeloid <- RunUMAP(myeloid, dims = 1:30, return.model = T)
# DefaultAssay(myeloid) <- "RNA"
# myeloid <- NormalizeData(myeloid)
# saveRDS(myeloid, "human_myeloid.rds")
# lymphoid <- subset(immune, idents = c("hTcell1", "hTcell2", "hTreg", "hBcell", "hpDC", "hPlasmablast", "hNK"))
# DefaultAssay(lymphoid) <- "integrated"
# lymphoid <- RunPCA(lymphoid)
# lymphoid <- RunUMAP(lymphoid, dims = 1:30, return.model = T)
# DefaultAssay(lymphoid) <- "RNA"
# lymphoid <- NormalizeData(lymphoid)
# saveRDS(lymphoid, "human_lymphoid.rds")

########################## ALL-CELL ##########################
cat_time("Annotating Seurat Object\n")
seurat <- readRDS("./seurat/human/all/nCells_2/nUMIs_400/mito_0.1/batch_default/var_genes_5000/int_method_RPCA/PCs_50/res_0.5/seurat.rds")

seurat$cell_type <- "temp"
seurat$cell_type[mast] <- "hMast"
seurat$cell_type[bcell1] <- "hBcell"
seurat$cell_type[bcell2] <- "hpDC"
seurat$cell_type[bcell3] <- "hPlasmablast"
seurat$cell_type[mac1] <- "hMac1"
seurat$cell_type[mac2] <- "hMac2"
seurat$cell_type[mac3] <- "hMac3"
seurat$cell_type[mono] <- "hMono1"
seurat$cell_type[DC1] <- "hcDC2"
seurat$cell_type[DC2] <- "hcDC1"
seurat$cell_type[DC3] <- "hASDC"
seurat$cell_type[mono2] <- "hMono2"
seurat$cell_type[t1] <- "hTcell1"
seurat$cell_type[t2] <- "hTcell2"
seurat$cell_type[t3] <- "hTreg"
seurat$cell_type[nk] <- "hNK"
seurat$cell_type[neu] <- "hNeu"
seurat$cell_type[sm1] <- "hSMC1"
seurat$cell_type[sm2] <- "hSMC2"
seurat$cell_type[peri] <- "hPeri"
seurat$cell_type[lec] <- "hLEC1"
seurat$cell_type[lec2] <- "hLEC2"
seurat$cell_type[ven] <- "hEndoV"
seurat$cell_type[stalk1] <- "hEndoS1"
seurat$cell_type[stalk2] <- "hEndoS2"
seurat$cell_type[stalk3] <- "hEndoS3"
seurat$cell_type[art1] <- "hEndoA1"
seurat$cell_type[art2] <- "hEndoA2"
seurat$cell_type[mes1] <- "hMes1"
seurat$cell_type[mes2] <- "hMes2"
seurat$cell_type[mes3] <- "hMes3"
seurat$cell_type[pre1] <- "hASPC1"
seurat$cell_type[pre2] <- "hASPC2"
seurat$cell_type[pre3] <- "hASPC3"
seurat$cell_type[pre4] <- "hASPC4"
seurat$cell_type[pre5] <- "hASPC5"
seurat$cell_type[pre6] <- "hASPC6"
seurat$cell_type[ad1] <- "hAd1"
seurat$cell_type[ad2] <- "hAd2"
seurat$cell_type[ad3] <- "hAd3"
seurat$cell_type[ad4] <- "hAd4"
seurat$cell_type[ad5] <- "hAd5"
seurat$cell_type[ad6] <- "hAd6"
seurat$cell_type[ad7] <- "hAd7"
seurat$cell_type[endomet] <- "hEndM"

seurat$cell_type2 <- "temp"
seurat$cell_type2[sm1] <- "SMC"
seurat$cell_type2[sm2] <- "SMC"
seurat$cell_type2[peri] <- "pericyte"
seurat$cell_type2[lec] <- "LEC"
seurat$cell_type2[lec2] <- "LEC"
seurat$cell_type2[ven] <- "endothelial"
seurat$cell_type2[stalk1] <- "endothelial"
seurat$cell_type2[stalk2] <- "endothelial"
seurat$cell_type2[stalk3] <- "endothelial"
seurat$cell_type2[art1] <- "endothelial"
seurat$cell_type2[art2] <- "endothelial"
seurat$cell_type2[mast] <- "mast_cell"
seurat$cell_type2[bcell1] <- "b_cell"
seurat$cell_type2[bcell2] <- "b_cell"
seurat$cell_type2[bcell3] <- "b_cell"
seurat$cell_type2[mac1] <- "macrophage"
seurat$cell_type2[mac2] <- "macrophage"
seurat$cell_type2[mac3] <- "macrophage"
seurat$cell_type2[mono] <- "monocyte"
seurat$cell_type2[DC1] <- "dendritic_cell"
seurat$cell_type2[DC2] <- "dendritic_cell"
seurat$cell_type2[DC3] <- "dendritic_cell"
seurat$cell_type2[mono2] <- "monocyte"
seurat$cell_type2[neu] <- "neutrophil"
seurat$cell_type2[t1] <- "t_cell"
seurat$cell_type2[t2] <- "t_cell"
seurat$cell_type2[t3] <- "t_cell"
seurat$cell_type2[nk] <- "nk_cell"
seurat$cell_type2[ad1] <- "adipocyte"
seurat$cell_type2[ad2] <- "adipocyte"
seurat$cell_type2[ad3] <- "adipocyte"
seurat$cell_type2[ad4] <- "adipocyte"
seurat$cell_type2[ad5] <- "adipocyte"
seurat$cell_type2[ad6] <- "adipocyte"
seurat$cell_type2[ad7] <- "adipocyte"
seurat$cell_type2[mes1] <- "mesothelium"
seurat$cell_type2[mes2] <- "mesothelium"
seurat$cell_type2[mes3] <- "mesothelium"
seurat$cell_type2[pre1] <- "ASPC"
seurat$cell_type2[pre2] <- "ASPC"
seurat$cell_type2[pre3] <- "ASPC"
seurat$cell_type2[pre4] <- "ASPC"
seurat$cell_type2[pre5] <- "ASPC"
seurat$cell_type2[pre6] <- "ASPC"
seurat$cell_type2[endomet] <- "endometrium"

seurat$ct3 <- seurat$cell_type2
seurat$ct3[ad1] <- "hAd1"
seurat$ct3[ad2] <- "hAd2"
seurat$ct3[ad3] <- "hAd3"
seurat$ct3[ad4] <- "hAd4"
seurat$ct3[ad5] <- "hAd5"
seurat$ct3[ad6] <- "hAd6"
seurat$ct3[ad7] <- "hAd7"

cells <- WhichCells(seurat, expression = cell_type == "temp", invert = TRUE)
seurat <- subset(seurat, cells = cells)
DefaultAssay(seurat) <- "integrated"
seurat <- RunPCA(seurat)
seurat <- RunUMAP(seurat, dims = 1:40)
Idents(seurat) <- "cell_type2"
DefaultAssay(seurat) <- "RNA"
seurat <- NormalizeData(seurat)
seurat$bmi2 <- "Over 30"
cells <- WhichCells(seurat, expression = bmi < 30)
seurat$bmi2[cells] <- "Below 30"
catf("Number of Cells in Whole Object: %s\n",as.character(dim(seurat)[2]))
saveRDS(seurat, "human_all.rds")
# pdf("plots.pdf")
# 	print(UMAPPlot(seurat, group.by = "cell_type", label = TRUE) + NoLegend())
# 	print(UMAPPlot(seurat, group.by = "cell_type2", label = TRUE))
# 	print(UMAPPlot(immune, group.by = "cell_type", label = TRUE))
# 	print(UMAPPlot(endo, group.by = "cell_type", label = TRUE))
# 	print(UMAPPlot(mes, group.by = "cell_type", label = TRUE))
# 	print(UMAPPlot(preads, group.by = "cell_type", label = TRUE))
# 	print(UMAPPlot(ads, group.by = "cell_type", label = TRUE))
# invisible(dev.off())

########################## SUB-OBJECTS ##########################
# seurat <- readRDS("human_all.rds")
DefaultAssay(seurat) <- "integrated"
seurat <- RunPCA(seurat)
seurat <- RunUMAP(seurat, dims = 1:50)
DefaultAssay(seurat) <- "RNA"
seurat <- NormalizeData(seurat)
saveRDS(seurat, "human_all.rds")

cells <- WhichCells(seurat, expression = technology == "Drop-Seq")
temp <- subset(seurat, cells = cells)
DefaultAssay(temp) <- "integrated"
temp <- RunPCA(temp)
temp <- RunUMAP(temp, dims = 1:50)
DefaultAssay(temp) <- "RNA"
# drop1 <- UMAPPlot(temp, group.by = "cell_type", label = TRUE)
# drop2 <- UMAPPlot(temp, group.by = "cell_type2", label = TRUE)
catf("Number of Drop-Seq cells: %s\n",as.character(dim(temp)[2]))
saveRDS(temp, "human_drop.rds")

cells <- WhichCells(seurat, cells = cells, invert = TRUE)
temp <- subset(seurat, cells = cells)
DefaultAssay(temp) <- "integrated"
temp <- RunPCA(temp)
temp <- RunUMAP(temp, dims = 1:50)
DefaultAssay(temp) <- "RNA"
# snuc1 <- UMAPPlot(temp, group.by = "cell_type", label = TRUE)
# snuc2 <- UMAPPlot(temp, group.by = "cell_type2", label = TRUE)
catf("Number of sNuc cells: %s\n",as.character(dim(temp)[2]))
saveRDS(temp, "human_snuc.rds")

meta <- cbind(temp$cell_type2, temp$cell_type)
colnames(meta) <- c("level1", "level2")
make_dir("./cellect")("./cellect")
write.csv(meta, "./cellect/human-meta.csv")
# data <- temp@assays$RNA@counts
# write(colnames(data), file = "hcolnames.txt")
# write(rownames(data), file = "hrownames.txt")
# writeMM(data, file = "human-counts.txt")

cells <- WhichCells(temp, expression = depot == "SAT")
temp2 <- subset(temp, cells = cells)
DefaultAssay(temp2) <- "integrated"
temp2 <- RunPCA(temp2)
temp2 <- RunUMAP(temp2, dims = 1:50)
DefaultAssay(temp2) <- "RNA"
# snuc.sat1 <- UMAPPlot(temp2, group.by = "cell_type", label = TRUE)
# snuc.sat2 <- UMAPPlot(temp2, group.by = "cell_type2", label = TRUE)
catf("Number of snuc sat cells: %s\n",as.character(dim(temp2)[2]))
saveRDS(temp2, "human_snuc_sat.rds")

cells <- WhichCells(temp, cells = cells, invert = TRUE)
temp2 <- subset(temp, cells = cells)
DefaultAssay(temp2) <- "integrated"
temp2 <- RunPCA(temp2)
temp2 <- RunUMAP(temp2, dims = 1:50)
DefaultAssay(temp2) <- "RNA"
# snuc.vat1 <- UMAPPlot(temp2, group.by = "cell_type", label = TRUE)
# snuc.vat2 <- UMAPPlot(temp2, group.by = "cell_type2", label = TRUE)
catf("Number of snuc vat cells: %s\n",as.character(dim(temp2)[2]))
saveRDS(temp2, "human_snuc_vat.rds")

cells <- WhichCells(seurat, expression = depot == "SAT")
temp <- subset(seurat, cells = cells)
DefaultAssay(temp) <- "integrated"
temp <- RunPCA(temp)
temp <- RunUMAP(temp, dims = 1:50)
DefaultAssay(temp) <- "RNA"
# sat1 <- UMAPPlot(temp, group.by = "cell_type", label = TRUE)
# sat2 <- UMAPPlot(temp, group.by = "cell_type2", label = TRUE)
catf("Number of SAT cells: %s\n",as.character(dim(temp)[2]))
saveRDS(temp, "human_sat.rds")

# pdf("subset-umaps.pdf")
# 	print(drop1 + ggtitle("Drop-seq"))
# 	print(drop2 + ggtitle("Drop-seq"))
# 	print(snuc1 + ggtitle("sNuc-seq"))
# 	print(snuc2 + ggtitle("sNuc-seq"))
# 	print(snuc.sat1 + ggtitle("sNuc-seq SAT"))
# 	print(snuc.sat2 + ggtitle("sNuc-seq SAT"))
# 	print(snuc.vat1 + ggtitle("sNuc-seq VAT"))
# 	print(snuc.vat2 + ggtitle("sNuc-seq VAT"))
# 	print(sat1 + ggtitle("SAT all"))
# 	print(sat2 + ggtitle("SAT all"))
# invisible(dev.off())
